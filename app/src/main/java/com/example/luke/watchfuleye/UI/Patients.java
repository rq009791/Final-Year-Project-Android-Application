package com.example.luke.watchfuleye.UI;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.luke.watchfuleye.R;
import com.example.luke.watchfuleye.utility.ServerConnectionThread;
import com.example.luke.watchfuleye.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.HashMap;

public class Patients extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patients);

        HashMap<String,String> queryParameters = ServerConnectionThread.getBasicQueryParameters();

        queryParameters.put("Relation","M");

        try {

            ServerConnectionThread serverConnectionThread = new ServerConnectionThread("Monitored-Users",queryParameters, null, ServerConnectionThread.HttpMethodType.GET, true);
            serverConnectionThread.start();

            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            LinearLayout vllPatientsButtonsArea = (LinearLayout)findViewById(R.id.vllPatientsButtonArea) ;

            serverConnectionThread.join(); // wait until the server respons
            final int serverStatusCode = serverConnectionThread.getResponseCode();

            switch (serverStatusCode){
                case 200:
                    JSONArray resultsArray = new JSONArray(serverConnectionThread.getResponse());

                    for(int counter = 0; counter < resultsArray.length(); counter++){
                        final JSONObject object = (resultsArray.getJSONObject(counter));
                        final int userID = object.getInt("Monitored_UserID");
                        Button btnUser = new Button(this);
                        btnUser.setText("" + userID);
                        btnUser.setOnClickListener(e -> {
                            /*alertDialog.setTitle("Button Clicked");
                            alertDialog.setMessage("User ID of " + userID + " was clicked!");
                            alertDialog.show();*/
                            Intent patientsMenu = new Intent(this,PatientOptionsView.class);
                            patientsMenu.putExtra("UserID", userID);
                            startActivity(patientsMenu);
                        });
                        vllPatientsButtonsArea.addView(btnUser);
                    }
                    /*alertDialog.setTitle("Server Response");
                    alertDialog.setMessage(resultsArray.toString());
                    alertDialog.show();*/
                    break;
                default:
                    Utility.displayServerError(serverConnectionThread, this);
                    break;
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
