package com.example.luke.watchfuleye.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.luke.watchfuleye.R;
import com.example.luke.watchfuleye.utility.ServerConnectionThread;
import com.example.luke.watchfuleye.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.HashMap;

public class Connections extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connections);

        HashMap<String,String> queryParams = ServerConnectionThread.getBasicQueryParameters();

        try {
            ServerConnectionThread serverConnectionThread = new ServerConnectionThread("Authorised-Users",queryParams,null, ServerConnectionThread.HttpMethodType.GET, true);
            serverConnectionThread.start();

            LinearLayout vllConnectionsArea = (LinearLayout)findViewById(R.id.vllConnectionsArea);

            HashMap<Integer,String> knownConnectionsLookup = LoginScreen.readConnections();
            // make sure you wait for the server to return data
            serverConnectionThread.join();
            int statusCode = serverConnectionThread.getResponseCode();
            switch (statusCode){
                case 200:
                    JSONArray connectionsJsonArray = new JSONArray(serverConnectionThread.getResponse());

                    for (int counter = 0; counter < connectionsJsonArray.length(); counter++){
                        JSONObject connectionObject = connectionsJsonArray.getJSONObject(counter);

                        Button btnConnection = new Button(this);
                        final int authorisedUserID = connectionObject.getInt("Authorised_UserID");
                        if(knownConnectionsLookup.containsKey(authorisedUserID)){
                            btnConnection.setText(knownConnectionsLookup.get(authorisedUserID));
                        }else{
                            btnConnection.setText(Integer.toString(authorisedUserID));
                        }

                        btnConnection.setOnClickListener(e -> {
                            Intent editConnection = new Intent(this, EditConnections.class);
                            editConnection.putExtra("UserID", authorisedUserID);
                            startActivity(editConnection);
                        });

                        vllConnectionsArea.addView(btnConnection);

                    }
                    break;
                default:
                    Utility.displayServerError(serverConnectionThread,this);
                    break;
            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
