package com.example.luke.watchfuleye.UI;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.example.luke.watchfuleye.R;
import com.example.luke.watchfuleye.datastructures.AuthorisedSettings;
import com.example.luke.watchfuleye.datastructures.GpsLocation;

public class SettingsActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        final EditText txtLatitude = (EditText)findViewById(R.id.txtLatitude);
        final EditText txtLongiitude  = (EditText)findViewById(R.id.txtLongitude);

        final Switch allowMovementAndHeartRateReadings = (Switch) findViewById(R.id.swiAllowMovementAndHeartrate);
        final Switch anonymiseGpsData = (Switch)findViewById(R.id.swiAnonymiseGpsData);
        final Switch gpsDataSwitch = (Switch) findViewById(R.id.swiAllowGpsRecordings);

        gpsDataSwitch.setOnCheckedChangeListener((CompoundButton compoundButton, boolean isChecked) -> {
            TextView lblHomeLocation = (TextView) findViewById(R.id.lblHomeLocation);
            LinearLayout hllLongitude = (LinearLayout) findViewById(R.id.hllLongitude);
            LinearLayout hllLatitude = (LinearLayout) findViewById(R.id.hllLatitude);
            Button btnUseCurrentLocation = (Button) findViewById(R.id.btnUseCurrentLocation);
            Switch swiAnonymiseGpsData = (Switch)findViewById(R.id.swiAnonymiseGpsData);

            if (isChecked) {
                lblHomeLocation.setVisibility(View.VISIBLE);
                hllLongitude.setVisibility(View.VISIBLE);
                hllLatitude.setVisibility(View.VISIBLE);
                btnUseCurrentLocation.setVisibility(View.VISIBLE);
                swiAnonymiseGpsData.setVisibility(View.VISIBLE);
            } else {
                lblHomeLocation.setVisibility(View.GONE);
                hllLongitude.setVisibility(View.GONE);
                hllLatitude.setVisibility(View.GONE);
                btnUseCurrentLocation.setVisibility(View.GONE);
                swiAnonymiseGpsData.setVisibility(View.GONE);
            }

        });
        // get the current application settings
        AuthorisedSettings applicationAuthorisedSettings = AuthorisedSettings.getInstance();
        // setup the controls appropriately
        allowMovementAndHeartRateReadings.setChecked(applicationAuthorisedSettings.isAllowMovementAndHeartRateRecordings());
        anonymiseGpsData.setChecked(applicationAuthorisedSettings.isAnnonymiseGpsData());
        gpsDataSwitch.setChecked(applicationAuthorisedSettings.isAllowGpsRecordings());

        GpsLocation homeLocation = applicationAuthorisedSettings.getHomeLocation();
        if(homeLocation != null){
            txtLatitude.setText("" + homeLocation.getLatitude());
            txtLongiitude.setText("" + homeLocation.getLongitude());
        }
    }

    public void useMyCurrentLocation(View view) {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location gpsLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        EditText txtLongitude = (EditText)findViewById(R.id.txtLongitude);
        EditText txtLatitude = (EditText)findViewById(R.id.txtLatitude);

        txtLongitude.setText(String.valueOf(gpsLocation.getLongitude()));
        txtLatitude.setText(String.valueOf(gpsLocation.getLatitude()));
    }


    public void submitSettings(View view){
        AuthorisedSettings authorisedSettings = AuthorisedSettings.getInstance();
        Switch allowGpsRecording = (Switch) findViewById(R.id.swiAllowGpsRecordings);
        Switch allowMovementAndHeartrate = (Switch) findViewById(R.id.swiAllowMovementAndHeartrate);
        Switch anonymiseGpsData = (Switch)findViewById(R.id.swiAnonymiseGpsData);

        EditText homeLocationLongitude = (EditText)findViewById(R.id.txtLongitude);
        EditText homeLocationLatitude = (EditText)findViewById(R.id.txtLatitude);

        String longitude = homeLocationLongitude.getText().toString();
        String latitude = homeLocationLatitude.getText().toString();

        if(longitude.equals("") || latitude.equals("")){
            authorisedSettings.setHomeLocation(null);
        }else{
            authorisedSettings.setHomeLocation(new GpsLocation(Double.valueOf(longitude), Double.valueOf(latitude)));
        }

        authorisedSettings.setAnnonymiseGpsData(anonymiseGpsData.isChecked());

        authorisedSettings.setAllowGpsRecordings(allowGpsRecording.isChecked());
        authorisedSettings.setAllowMovementAndHeartRateRecordings(allowMovementAndHeartrate.isChecked());

        // persist the settings change
        LoginScreen.updateUserSettings(authorisedSettings.getUserSettings());

        // send user to the main menu
        Intent mainMenu = new Intent(this, MainMenu.class);
        startActivity(mainMenu);
    }
}
