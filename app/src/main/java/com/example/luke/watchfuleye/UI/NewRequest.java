package com.example.luke.watchfuleye.UI;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.luke.watchfuleye.R;
import com.example.luke.watchfuleye.utility.ServerConnectionThread;
import com.example.luke.watchfuleye.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.HashMap;

public class NewRequest extends AppCompatActivity {
    private HashMap<Integer,String> ruleParameters;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_request);

        ruleParameters = new HashMap<>();

        Spinner spnRequestType = (Spinner)findViewById(R.id.spnRequestType);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.request_type_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spnRequestType.setAdapter(adapter);




        Spinner spnRuleAction = (Spinner) findViewById(R.id.spnRuleAction);
        ArrayAdapter<CharSequence> ruleActionAdapter = ArrayAdapter.createFromResource(this,
                R.array.rule_actions, android.R.layout.simple_spinner_item);
        ruleActionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spnRuleAction.setAdapter(ruleActionAdapter);


        Spinner spnRelation = (Spinner)findViewById(R.id.spnRelation);
        ArrayAdapter relationAdapter = ArrayAdapter.createFromResource(this, R.array.relations, android.R.layout.simple_spinner_item);
        relationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnRelation.setAdapter(relationAdapter);

        LinearLayout vllRuleParameterArea = (LinearLayout)findViewById(R.id.vllRuleParamitersArea);

        // event handlers

        spnRequestType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String[] requestType = getResources().getStringArray(R.array.request_type_array);

                LinearLayout vllRuleSection = (LinearLayout)findViewById(R.id.vllRuleSection);
                LinearLayout hllRelation = (LinearLayout)findViewById(R.id.hllRelation);
                if(position == 3){
                    vllRuleSection.setVisibility(View.VISIBLE);
                    hllRelation.setVisibility(View.GONE);
                }else{
                    vllRuleSection.setVisibility(View.GONE);
                    hllRelation.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spnRuleAction.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // flush the rule parameters
                ruleParameters.clear();
                vllRuleParameterArea.removeAllViews();
                // construct the required parameters
                String value = (String) parent.getItemAtPosition(position);
                // generate the label
                LinearLayout hllParameterGroup = new LinearLayout(getApplicationContext());
                TextView lblText = new TextView(getApplicationContext());
                lblText.setTextSize(24);
                switch (value){


                    case "Above (Heart-Rate)":

                        lblText.setText("Above a value of: ");
                        EditText txtNumber = new EditText(getApplicationContext());
                        txtNumber.setInputType(InputType.TYPE_CLASS_NUMBER);
                        txtNumber.setWidth(500);
                        hllParameterGroup.addView(lblText);
                        hllParameterGroup.addView(txtNumber);
                        vllRuleParameterArea.addView(hllParameterGroup);
                        txtNumber.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                // do nothing
                            }

                            @Override
                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                // add to the hash map
                                ruleParameters.put(0, charSequence.toString());
                            }

                            @Override
                            public void afterTextChanged(Editable editable) {
                                //  do nothing
                            }
                        });
                        break;
                    case "Below (Heart-Rate)":
                        lblText.setText("Below a value of: ");
                        EditText txtNumber2 = new EditText(getApplicationContext());
                        txtNumber2.setInputType(InputType.TYPE_CLASS_NUMBER);
                        txtNumber2.setWidth(500);
                        hllParameterGroup.addView(lblText);
                        hllParameterGroup.addView(txtNumber2);
                        vllRuleParameterArea.addView(hllParameterGroup);
                        txtNumber2.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                // do nothing
                            }

                            @Override
                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                                // add to the hash map
                                ruleParameters.put(0, charSequence.toString());
                            }

                            @Override
                            public void afterTextChanged(Editable editable) {
                                //  do nothing
                            }
                        });
                        break;

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // do nothing
            }
        });

    }

    public void makeRequest(View view){
        Spinner requestType = (Spinner)findViewById(R.id.spnRequestType);
        Spinner spnRelation = (Spinner)findViewById(R.id.spnRelation);
        Spinner spnRuleAction = (Spinner)findViewById(R.id.spnRuleAction);
        EditText txtActionOn = (EditText)findViewById(R.id.txtActionOn);
        TextView txtRuleName = (EditText)findViewById(R.id.txtRuleName);
        int selectedRequestType = requestType.getSelectedItemPosition();

        // prepare a dialog box
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        // query parameters setup
        HashMap<String , String > queryParameters = ServerConnectionThread.getBasicQueryParameters();

        // creating base post body object
        JSONObject postBody =  new JSONObject();
        ServerConnectionThread serverConnectionThread = null;
        switch (selectedRequestType){
            case 0: // heart rate data

                try {

                    postBody.put("ActionOn", Integer.valueOf(txtActionOn.getText().toString()) );
                    postBody.put("DataSet", "H");
                    switch (spnRelation.getSelectedItemPosition()){
                        case 0:
                            postBody.put("Relation", "F");
                            break;
                        case 1:
                            postBody.put("Relation", "M");
                            break;
                    }

                    serverConnectionThread = new ServerConnectionThread("Request-Access",queryParameters,postBody, ServerConnectionThread.HttpMethodType.POST,true);



                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case 1: // Movement (Acceleromiter )
                try {

                    postBody.put("ActionOn", Integer.valueOf(txtActionOn.getText().toString()) );
                    postBody.put("DataSet", "A");
                    switch (spnRelation.getSelectedItemPosition()){
                        case 0:
                            postBody.put("Relation", "F");
                            break;
                        case 1:
                            postBody.put("Relation", "M");
                            break;
                    }

                    serverConnectionThread = new ServerConnectionThread("Request-Access",queryParameters,postBody, ServerConnectionThread.HttpMethodType.POST, true);


                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case 2: // GPS
                try {

                    postBody.put("ActionOn", Integer.valueOf(txtActionOn.getText().toString()) );
                    postBody.put("DataSet", "G");
                    switch (spnRelation.getSelectedItemPosition()){
                        case 0:
                            postBody.put("Relation", "F");
                            break;
                        case 1:
                            postBody.put("Relation", "M");
                            break;
                    }

                    serverConnectionThread = new ServerConnectionThread("Request-Access",queryParameters,postBody, ServerConnectionThread.HttpMethodType.POST,true);

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case 3:  // Rule

                try {
                    postBody.put("ActionOn", Integer.valueOf(txtActionOn.getText().toString()) );
                    postBody.put("Name",  txtRuleName.getText().toString());
                    switch ((String)spnRuleAction.getSelectedItem()){
                        case "Above (Heart-Rate)":
                            postBody.put("Command",  "ABV");
                            break;
                        case "Below (Heart-Rate)":
                            postBody.put("Command", "BLO" );
                            break;
                    }

                    postBody.put("DataTable", "H" );

                    JSONArray ruleParamsJson = new JSONArray();
                    JSONObject paramIndex1 = new JSONObject();
                    paramIndex1.put("Index", 0);
                    if(ruleParameters.containsKey(0)){
                        paramIndex1.put("Value", Integer.valueOf(ruleParameters.get(0)));
                        paramIndex1.put("Type", "INT");
                        ruleParamsJson.put(paramIndex1);

                        postBody.put("Parameters", ruleParamsJson );

                        serverConnectionThread = new ServerConnectionThread("New-Rule", queryParameters, postBody, ServerConnectionThread.HttpMethodType.POST, true);

                    }else{
                        alertDialog.setTitle("Error!");
                        alertDialog.setMessage("A rule parameter has not been set.");
                        alertDialog.show();
                    }

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }

        serverConnectionThread.start();
        try {
            serverConnectionThread.join();
            int statusCode = serverConnectionThread.getResponseCode();
            switch (statusCode){
                case 200:
                    alertDialog.setTitle("Success");
                    alertDialog.setMessage("The request has been made successfully.");
                    alertDialog.show();
                    break;
                default:
                    Utility.displayServerError(serverConnectionThread,getApplicationContext());
                    break;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
