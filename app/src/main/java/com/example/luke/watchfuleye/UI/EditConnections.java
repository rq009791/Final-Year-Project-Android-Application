package com.example.luke.watchfuleye.UI;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.luke.watchfuleye.R;
import com.example.luke.watchfuleye.utility.ServerConnectionThread;
import com.example.luke.watchfuleye.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;

public class EditConnections extends AppCompatActivity {
    private int connectionUserID;

    HashMap<Integer,Integer> authorisedUserPermissionIDsSelected = new HashMap<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_connections);

        Intent intent = getIntent();

        connectionUserID = intent.getIntExtra("UserID", -1);
        HashMap<String,String> queryParams = ServerConnectionThread.getBasicQueryParametersWithAuthentication();

        TextView lbluserID = (TextView)findViewById(R.id.lblEditConnectionUserID);

        // update the UI
        lbluserID.setText(Integer.toString(connectionUserID));



        HashMap<Integer,String> connections = LoginScreen.readConnections();
        // if the name exists then pre populate it
        if(connections.containsKey(connectionUserID)){
            TextView txtCommonName = (TextView)findViewById(R.id.txtCommonName);
            txtCommonName.setText(connections.get(connectionUserID));
        }

        refreshPrivileges();

    }

    private void refreshPrivileges(){
        // connect to the server to get the users permissions
        try {
            HashMap<String,String> queryParams = ServerConnectionThread.getBasicQueryParameters();
            ServerConnectionThread serverConnectionThread = new ServerConnectionThread("Connection/" + connectionUserID + "/Permissions",queryParams,null, ServerConnectionThread.HttpMethodType.GET, true);
            serverConnectionThread.start();
            // while the IO operation is underway do some other stuff
            LinearLayout vllPrivileges = (LinearLayout)findViewById(R.id.vllEditConnectionPrivileges);
            vllPrivileges.removeAllViews();

            serverConnectionThread.join(); // wait for the server response before continuing

            int statusCode = serverConnectionThread.getResponseCode();

            switch (statusCode){
                case 200:
                    JSONArray responseArray = new JSONArray(serverConnectionThread.getResponse());

                    for(int counter = 0; counter < responseArray.length(); counter++){

                        JSONObject jsonObject = responseArray.getJSONObject(counter);
                        String tableName = jsonObject.getString("DataTable");
                        // convert the system abbreviation for a more readable one
                        switch (tableName){
                            case "H":
                                tableName = "Heart-Rate";
                                break;
                            case "A":
                                tableName = "Movement";
                                break;
                            case "R":
                                tableName = "Rules";
                                break;
                            case "G":
                                tableName = "GPS";
                                break;
                            default:
                                tableName = "Error";
                                break;
                        }

                        final int authorisationRowID = jsonObject.getInt("Authorised_UsersID");
                        String timestamp = jsonObject.getString("Expiry_Time");
                        LinearLayout hllPrivilege = new LinearLayout(this);
                        hllPrivilege.setOrientation(LinearLayout.HORIZONTAL);
                        Button btnPrivilege = new Button(this);
                        btnPrivilege.setText(tableName);

                        CheckBox chkHighlightPrivilege = new CheckBox(this);
                        // set the event for changing the checkbox
                        chkHighlightPrivilege.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton compoundButton, boolean value) {
                                if(value){
                                    authorisedUserPermissionIDsSelected.put(authorisationRowID,authorisationRowID);
                                }else {
                                    if(authorisedUserPermissionIDsSelected.containsKey(authorisationRowID)){
                                        authorisedUserPermissionIDsSelected.remove(authorisationRowID);
                                    }
                                }
                            }
                        });
                        // add the controls to the horizontal layout
                        hllPrivilege.addView(btnPrivilege);
                        hllPrivilege.addView(chkHighlightPrivilege);


                        vllPrivileges.addView(hllPrivilege);
                    }

                    break;
                default:
                    Utility.displayServerError(serverConnectionThread,this);
                    break;
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void removePrivileges(View view){




        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        AlertDialog alertDialog = dialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ArrayList<ServerConnectionThread> serverConnections = new ArrayList();
                // remove from server
                for( int permissionID : authorisedUserPermissionIDsSelected.values()){
                    HashMap<String,String> queryParams = ServerConnectionThread.getBasicQueryParameters();
                    try {
                        ServerConnectionThread serverConnectionThread = new ServerConnectionThread("Connection/" + connectionUserID + "/Permissions/" + permissionID, queryParams, null, ServerConnectionThread.HttpMethodType.DELETE, true);
                        serverConnectionThread.start();

                        serverConnections.add(serverConnectionThread);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }

                }

                for(ServerConnectionThread thread :  serverConnections){
                    try {
                        thread.join();
                        int statusCode = thread.getResponseCode();

                        switch (statusCode){
                            case 200:
                                // success
                                break;
                            default:
                                Utility.displayServerError(thread,getApplicationContext());
                                break;
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                // reset the selected list
                authorisedUserPermissionIDsSelected.clear();
                // refresh page
                refreshPrivileges();


                dialogInterface.dismiss();
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).setTitle("Revoking Privileges").setMessage("You are about to remove privileges permanently. Are you sure you want to proceed?").create();
        alertDialog.show();
        // if the user still want to remove privileges then perform th delete operations




    }

    public void save(View view){

        TextView txtCommonName = (TextView)findViewById(R.id.txtCommonName);

        // save to file
        LoginScreen.updateConnections(connectionUserID, txtCommonName.getText().toString());

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        Dialog dialog = alertBuilder.setTitle("Success").setMessage("The changes have beeen saved successfuly.").create();
        dialog.show();

    }
}
