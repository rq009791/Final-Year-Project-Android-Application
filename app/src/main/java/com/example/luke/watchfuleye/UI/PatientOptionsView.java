package com.example.luke.watchfuleye.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.luke.watchfuleye.R;

public class PatientOptionsView extends AppCompatActivity {
    int userID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_options_view);

        Intent intent = getIntent();
        this.userID = intent.getIntExtra("UserID", -1);

    }

    public void onWarningsButtonClicked(View view){
        Intent warningsScreen = new Intent(this, Warnings.class);
        warningsScreen.putExtra("UserID", userID);
        startActivity(warningsScreen);
    }

    public void onHeartRateButtonclicked(View view){
        Intent graphView = new Intent(this, GraphActivity.class);
        graphView.putExtra("UserID", userID);
        graphView.putExtra("Mode", 'H');
        startActivity(graphView);
    }
    public void onMovementButtonClicked(View view){
        Intent graphView = new Intent(this, GraphActivity.class);
        graphView.putExtra("UserID", userID);
        graphView.putExtra("Mode", 'A');
        startActivity(graphView);
    }
}
