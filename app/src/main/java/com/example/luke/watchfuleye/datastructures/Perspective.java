package com.example.luke.watchfuleye.datastructures;

/**
 * Created by Luke on 18/01/2018.
 */

public enum Perspective {
    DEFAULT,
    FAMILY,
    MEDICAL_PROFESSIONAL
}
