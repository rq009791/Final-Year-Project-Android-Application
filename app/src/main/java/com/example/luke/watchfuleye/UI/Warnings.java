package com.example.luke.watchfuleye.UI;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.luke.watchfuleye.R;
import com.example.luke.watchfuleye.utility.ServerConnectionThread;
import com.example.luke.watchfuleye.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.HashMap;

public class Warnings extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_warnings);

        int userID = getIntent().getIntExtra("UserID", -1);
        if(userID != -1){ // if not error id
            HashMap<String,String> parameters = ServerConnectionThread.getBasicQueryParameters();

            try {
                ServerConnectionThread serverConnectionThread = new ServerConnectionThread("Warnings/" + userID, parameters, null, ServerConnectionThread.HttpMethodType.GET,true);

                serverConnectionThread.start();

                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                LinearLayout vllWarningsArea = (LinearLayout) findViewById(R.id.vllWarningsArea);
                serverConnectionThread.join();
                int responseCode = serverConnectionThread.getResponseCode();
                switch (responseCode){
                    case 200:
                        JSONArray result = new JSONArray(serverConnectionThread.getResponse());
                        TextView lblWarnings = new TextView(this);
                        lblWarnings.setTextSize(30);
                        lblWarnings.setText("Warnings");
                        lblWarnings.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

                        vllWarningsArea.addView(lblWarnings);
                        for(int counter = 0; counter < result.length() ; counter++){
                            final JSONObject object =  result.getJSONObject(counter);
                            /*LinearLayout hllButtonSection = new LinearLayout(this);
                            hllButtonSection.setOrientation(LinearLayout.HORIZONTAL);
                            TextView lblLabel = new TextView(this);
                            lblLabel.setText(object.toString());
                            */


                            final String warningTitle = object.getString("Title");
                            final String warningNotification = object.getString("Notification");
                            final String warningNotificationType = object.getString("Notification_Type");

                            Button btnWarning = new Button(this);
                            btnWarning.setText(warningTitle);
                            vllWarningsArea.addView(btnWarning);
                            // event handler
                            btnWarning.setOnClickListener(e-> {
                                /*alertDialog.setTitle("Button Click");
                                alertDialog.setMessage("Button clicked!" +object.toString());
                                alertDialog.show();*/
                                Intent warningView = new Intent(this, WarningView.class);
                                warningView.putExtra("Title", warningTitle );
                                warningView.putExtra("Notification", warningNotification);
                                warningView.putExtra("Notification_Type", warningNotificationType);

                                startActivity(warningView);
                            });
                        }
                        /*alertDialog.setTitle("Success");
                        alertDialog.setMessage(result.toString());
                        alertDialog.show();*/
                        break;
                    default:
                        Utility.displayServerError(serverConnectionThread,this);
                        break;
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
