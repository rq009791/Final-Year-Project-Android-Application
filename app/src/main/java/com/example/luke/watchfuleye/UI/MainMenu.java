package com.example.luke.watchfuleye.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.example.luke.watchfuleye.R;
import com.example.luke.watchfuleye.datastructures.AuthorisedSettings;
import com.example.luke.watchfuleye.datastructures.Perspective;

public class MainMenu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Perspective perspective = AuthorisedSettings.getInstance().getPerspective();

        //get buttons which may be hidden
        Button btnConnections = (Button)findViewById(R.id.btnConnections);
        Button btnFamily = (Button)findViewById(R.id.btnFamily);
        Button btnPatients = (Button)findViewById(R.id.btnPatients);
        if(perspective == Perspective.FAMILY || perspective == Perspective.MEDICAL_PROFESSIONAL){
            btnConnections.setVisibility(View.GONE);
        }
        if(perspective == Perspective.FAMILY || perspective == Perspective.DEFAULT){
            btnPatients.setVisibility(View.GONE);
        }
        if(perspective == Perspective.DEFAULT){
            btnFamily.setVisibility(View.GONE);
        }
    }


    public void settingButtonClicked(View view){
        Intent settingsScreen = new Intent(this,SettingsActivity.class);
        startActivity(settingsScreen);
    }


    public void requestsPendingApprovalButtonClicked(View view){
        Intent pendingAproval = new Intent(this,PendingAproval.class);
        startActivity(pendingAproval);
    }

    public void familyMembersButtonClicked(View view){
        Intent familyMembers = new Intent(this,FamilyMembers.class);
        startActivity(familyMembers);
    }

    public void patientsButtonClicked(View view){
        Intent patientsView = new Intent(this,Patients.class);
        startActivity(patientsView);
    }

    public void connectionsButtonClicked(View view){
        Intent connectionsView = new Intent(this,Connections.class);
        startActivity(connectionsView);
    }
}
