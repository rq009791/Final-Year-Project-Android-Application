package com.example.luke.watchfuleye.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.example.luke.watchfuleye.R;

public class WarningView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_warning_view);
        Intent intent = getIntent();
        String title = intent.getStringExtra("Title");
        String notification = intent.getStringExtra("Notification");
        String notificationType = intent.getStringExtra("Notification_Type");
        //String userID = intent.getStringExtra("UserID");
        if(notificationType != null){
            switch (notificationType){
                case "H":
                    notificationType = "Heartrate";
                    break;
                case "A":
                    notificationType = "Movement";
                    break;
                case "G":
                    notificationType = "GPS";
                    break;
                case "R":
                    notificationType = "Rule";
                    break;
                default:
                    notificationType = "";
                    break;
            }
        }


        EditText txtTitle = (EditText)findViewById(R.id.txtTitle);
        EditText txtType = (EditText)findViewById(R.id.txtNotificationType);
        EditText txtDetail = (EditText)findViewById(R.id.txtWarningDetail);


        txtTitle.setText(title);
        txtType.setText(notificationType);
        txtDetail.setText(notification);

    }
}
