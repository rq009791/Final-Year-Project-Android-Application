package com.example.luke.watchfuleye.utility;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Luke on 05/03/2018.
 */

public class Utility {
    public static void displayServerError(ServerConnectionThread serverConnection, Context context){
        try {
            JSONObject errorObject = new JSONObject(serverConnection.getResponse());
            AlertDialog alert = new AlertDialog.Builder(context).setTitle("Error!").setMessage(errorObject.getString("Message")).setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).create();
            alert.show();
        } catch (JSONException e) {

            AlertDialog alert = new AlertDialog.Builder(context).setTitle("Error!").setMessage("Unexpected response from server received.").setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).create();
            alert.show();
        }
    }
}
