package com.example.luke.watchfuleye.UI;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.luke.watchfuleye.R;
import com.example.luke.watchfuleye.utility.ServerConnectionThread;
import com.example.luke.watchfuleye.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.HashMap;

public class AccessRequestView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_access_request_view);

        Intent intent = getIntent();
        boolean isMonitoredUser = intent.getBooleanExtra("IsMonitoredUser",false);
        int recordID = intent.getIntExtra("RecordID", -1);
        int monitoredUserID = intent.getIntExtra("MonitoredUserID", -1);
        int authorisedUserID = intent.getIntExtra("AuthorisedUserID", -1 );
        String relation = intent.getStringExtra("Relation");
        String dataTableFinal = intent.getStringExtra("DataTable" );


        TextView lblActionOn = (TextView)findViewById(R.id.lblAccessRequestActionOn);
        TextView lblRequestedBy = (TextView)findViewById(R.id.lblAccessRequestRequestedBy);
        TextView lblRelation = (TextView)findViewById(R.id.lblAccessRequestRelation);
        TextView lbldataTable = (TextView)findViewById(R.id.lblAccessRequestTable);

        lblActionOn.setText("Action On:\t" + monitoredUserID);
        lblRequestedBy.setText("Requested By:\t" + authorisedUserID);
        lblRelation.setText("Relation:\t" + relation);
        lbldataTable.setText("Access requested to:\t" +  dataTableFinal);

        LinearLayout vllDisplayArea = (LinearLayout)findViewById(R.id.vllAccessRequestViewArea);

        if(isMonitoredUser){
            Button btnApprove = new Button(this);
            Button btnDeny = new Button(this);

            // set the text of the buttons
            btnApprove.setText("Approve");
            btnDeny.setText("Deny");

            // set colours
            btnApprove.setBackgroundColor(Color.GREEN);
            btnDeny.setBackgroundColor(Color.RED);
            final Context viewContext = this;
            btnApprove.setOnClickListener(e ->{
                HashMap<String,String> queryParams = ServerConnectionThread.getBasicQueryParameters();

                JSONObject httpBody = new JSONObject();

                try {
                    httpBody.put("UserIdToAuthorise", authorisedUserID);
                    httpBody.put("RequestID", recordID);

                    ServerConnectionThread serverConnectionThread = new ServerConnectionThread("Approve-Access-Request",queryParams,httpBody, ServerConnectionThread.HttpMethodType.POST,true);

                    serverConnectionThread.start();
                    serverConnectionThread.join();

                    int statusCode = serverConnectionThread.getResponseCode();

                    switch (statusCode){
                        case 200:
                            // go back to the main request display
                            Intent pendingApproval = new Intent(this, PendingAproval.class);
                            startActivity(pendingApproval);
                            break;
                        default:
                            Utility.displayServerError(serverConnectionThread,viewContext);
                            break;
                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                } catch (MalformedURLException e1) {
                    e1.printStackTrace();
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }

            });
            btnDeny.setOnClickListener(e -> {
                HashMap<String,String> queryParams = ServerConnectionThread.getBasicQueryParameters();

                try {


                    ServerConnectionThread serverConnectionThread = new ServerConnectionThread("Reject-Access-Request/" + recordID,queryParams,null, ServerConnectionThread.HttpMethodType.DELETE, true);

                    serverConnectionThread.start();
                    serverConnectionThread.join();

                    int statusCode = serverConnectionThread.getResponseCode();

                    switch (statusCode){
                        case 200:
                            // go back to the main request display
                            Intent pendingApproval = new Intent(this, PendingAproval.class);
                            startActivity(pendingApproval);
                            break;
                        default:
                            Utility.displayServerError(serverConnectionThread,viewContext);
                            break;
                    }
                } catch (MalformedURLException e1) {
                    e1.printStackTrace();
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            });

            // add to the layout
            vllDisplayArea.addView(btnApprove);
            vllDisplayArea.addView(btnDeny);

        }

    }
}
