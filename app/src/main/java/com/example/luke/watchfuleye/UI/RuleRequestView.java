package com.example.luke.watchfuleye.UI;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.luke.watchfuleye.R;
import com.example.luke.watchfuleye.utility.ServerConnectionThread;
import com.example.luke.watchfuleye.utility.Utility;

import java.net.MalformedURLException;
import java.util.HashMap;

public class RuleRequestView extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rule_request_view);

        Intent intent = getIntent();

        boolean isMonitoredUser = intent.getBooleanExtra("IsMonitoredUser", false);
        final int ruleID = intent.getIntExtra("RuleID", -1);
        String ruleName = intent.getStringExtra("RuleName");
        String actionName = intent.getStringExtra("ActionName");
        int actionOn = intent.getIntExtra("ActionOn", -1 );
        int createdBy = intent.getIntExtra("CreatedBy", -1);
        boolean authorised = intent.getBooleanExtra("Authorised", false);
        String dataTable = intent.getStringExtra("DataTable");

        // fetch the UI controls
        TextView lblRuleName = (TextView)findViewById(R.id.lblRuleName);
        TextView lblRuleAction = (TextView)findViewById(R.id.lblAction);
        TextView lblActionOn = (TextView)findViewById(R.id.lblActionOn);
        TextView lblCreatedBy = (TextView)findViewById(R.id.lblCreatedBy);
        TextView lblDataTable = (TextView)findViewById(R.id.lblDataTable);
        LinearLayout vllLayoutArea = (LinearLayout)findViewById(R.id.vllRuleRequestViewArea);
        // set the UI controls
        lblActionOn.setText("Action On:\t" + Integer.toString(actionOn));
        lblCreatedBy.setText("Created By:\t" + Integer.toString(createdBy));
        lblRuleName.setText("Rule Name:\t" + ruleName);
        lblRuleAction.setText("Action:\t" + actionName);
        lblDataTable.setText("Data Applied To: " + dataTable);

        if(isMonitoredUser){
            lblActionOn.setVisibility(View.GONE);
            Button btnApprove = new Button(this);
            Button btnDeny = new Button(this);

            btnApprove.setText("Approve");
            btnDeny.setText("Deny");

            btnApprove.setBackgroundColor(Color.GREEN);
            btnDeny.setBackgroundColor(Color.RED);

            btnApprove.setOnClickListener(e -> {
                HashMap<String,String> queryParams = ServerConnectionThread.getBasicQueryParameters();
                try {
                    ServerConnectionThread serverConnectionThread = new ServerConnectionThread("Approve-Rule/" + ruleID ,queryParams,null, ServerConnectionThread.HttpMethodType.POST, true);
                    serverConnectionThread.start();

                    serverConnectionThread.join();

                    int statusCode = serverConnectionThread.getResponseCode();

                    switch (statusCode){
                        case 200:
                            Intent pendingAprovals = new Intent(this, PendingAproval.class);
                            startActivity(pendingAprovals);
                            break;
                        default:
                            Utility.displayServerError(serverConnectionThread, getApplicationContext());
                            break;
                    }
                } catch (MalformedURLException e1) {
                    e1.printStackTrace();
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            });

            btnDeny.setOnClickListener(e -> {
                HashMap<String,String> queryParams = ServerConnectionThread.getBasicQueryParameters();
                try {
                    ServerConnectionThread serverConnectionThread = new ServerConnectionThread("Reject-Rule/" + ruleID ,queryParams,null, ServerConnectionThread.HttpMethodType.DELETE,true);
                    serverConnectionThread.start();

                    serverConnectionThread.join();

                    int statusCode = serverConnectionThread.getResponseCode();

                    switch (statusCode){
                        case 200:
                            Intent pendingAprovals = new Intent(this, PendingAproval.class);
                            startActivity(pendingAprovals);
                            break;
                        default:
                            Utility.displayServerError(serverConnectionThread, getApplicationContext());
                            break;
                    }
                } catch (MalformedURLException e1) {
                    e1.printStackTrace();
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            });

            // add the controls to the master layout
            vllLayoutArea.addView(btnApprove);
            vllLayoutArea.addView(btnDeny);
        }else{
            lblCreatedBy.setVisibility(View.GONE);
        }

    }
}
