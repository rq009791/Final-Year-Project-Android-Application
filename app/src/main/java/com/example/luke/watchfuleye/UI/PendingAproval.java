package com.example.luke.watchfuleye.UI;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.luke.watchfuleye.R;
import com.example.luke.watchfuleye.datastructures.AuthorisedSettings;
import com.example.luke.watchfuleye.datastructures.Perspective;
import com.example.luke.watchfuleye.utility.ServerConnectionThread;
import com.example.luke.watchfuleye.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.HashMap;

public class PendingAproval extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_aproval);
        AuthorisedSettings settings = AuthorisedSettings.getInstance();
        if(settings.getPerspective() == null) {
            refreshPage(false);
        }else if(settings.getPerspective().equals(Perspective.DEFAULT)){
            refreshPage(true);
        }else{
            refreshPage(false);
        }

        Button btnNewRequest = (Button)findViewById(R.id.btnNewRequest);

        Perspective perspective = AuthorisedSettings.getInstance().getPerspective();
        if(perspective == Perspective.DEFAULT){
            btnNewRequest.setVisibility(View.GONE);
        }
    }

    public void newRequest(View view){
        Intent newRequestIntent = new Intent(this,NewRequest.class);
        startActivity(newRequestIntent);
    }

    public void refreshPage(boolean isMonitoredUser){
        LinearLayout vllRequestsArea = (LinearLayout) findViewById(R.id.vllRequestArea);

        ServerConnectionThread pendingAprovalThread;
        try {
            HashMap<String,String> queryParameters = ServerConnectionThread.getBasicQueryParameters();
            pendingAprovalThread = new ServerConnectionThread("My-Requests",queryParameters, null, ServerConnectionThread.HttpMethodType.GET, true);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return;
        }
        pendingAprovalThread.start();

        try {
            pendingAprovalThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return;
        }

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        int statusCode = pendingAprovalThread.getResponseCode();
        switch (statusCode){
            case 200:
                try {
                    JSONObject response = new JSONObject(pendingAprovalThread.getResponse());
                   /* alertDialog.setTitle("Response");
                    alertDialog.setMessage(response.toString());
                    alertDialog.show();*/

                    JSONArray accessRequests = (JSONArray) response.get("AccessRequests");
                    vllRequestsArea.removeAllViews();
                    for (int x = 0; x < accessRequests.length(); x ++){

                        JSONObject responseObject = accessRequests.getJSONObject(x);
                        /*
                        LinearLayout hllAccessRequestFrame = new LinearLayout(this);
                        hllAccessRequestFrame.setOrientation(LinearLayout.HORIZONTAL);

                        TextView textView = new TextView(this);
                        textView.setText(.toString());
                        */
                        int recordID = responseObject.getInt("Authorised_UsersID");
                        Object authorisedTime = responseObject.get("Authorised_Time");
                        String authorisedTimeString = null;
                        if(!authorisedTime.equals(JSONObject.NULL)){
                            authorisedTimeString = (String)authorisedTime;
                        }

                        final int monitoredUserID = responseObject.getInt("Monitored_UserID");
                        final int authorisedUserID = responseObject.getInt("Authorised_UserID");
                        String relation = responseObject.getString("Relation");
                        String dataTable = responseObject.getString("DataTable");
                        switch (dataTable){
                            case "R":
                                dataTable = "Rule";
                                break;
                            case "H":
                                dataTable = "Heart-Rate";
                                break;
                            case "A":
                                dataTable = "Movement";
                                break;
                            case "G":
                                dataTable = "GPS";
                                break;
                        }
                        final String dataTableFinal = dataTable;

                        Button btnRequest = new Button(this);
                        if(isMonitoredUser){
                            btnRequest.setText(Integer.toString(authorisedUserID) + ":\tAccess Request to " + dataTable + " data." );
                        }else {
                            btnRequest.setText(Integer.toString(monitoredUserID) + ":\tAccess Request to " + dataTable + " data.");
                        }

                        btnRequest.setOnClickListener(e -> {
                            Intent accessRequestView = new Intent(this, AccessRequestView.class);
                            accessRequestView.putExtra("IsMonitoredUser",isMonitoredUser);
                            accessRequestView.putExtra("RecordID", recordID);
                            accessRequestView.putExtra("MonitoredUserID", monitoredUserID);
                            accessRequestView.putExtra("AuthorisedUserID", authorisedUserID);
                            accessRequestView.putExtra("Relation", relation);
                            accessRequestView.putExtra("DataTable", dataTableFinal);

                            startActivity(accessRequestView);
                        });


                        vllRequestsArea.addView(btnRequest);

                    }

                    JSONArray ruleRequests = response.getJSONArray("RuleRequests");

                    for(int counter = 0; counter < ruleRequests.length(); counter++){

                        JSONObject jsonRuleRequest = ruleRequests.getJSONObject(counter);

                        int ruleID = jsonRuleRequest.getInt("RuleID");
                        String ruleName = jsonRuleRequest.getString("Rule_Name");
                        String actionName = jsonRuleRequest.getString("Action_Name");
                        int actionOn = jsonRuleRequest.getInt("Action_On");
                        int createdBy = jsonRuleRequest.getInt("Created_By");
                        boolean authorised = false;
                        String dataTable = jsonRuleRequest.getString("DataTable");
                        switch (dataTable){
                            case "R":
                                dataTable = "Rule";
                                break;
                            case "H":
                                dataTable = "Heart-Rate";
                                break;
                            case "A":
                                dataTable = "Movement";
                                break;
                            case "G":
                                dataTable = "GPS";
                                break;
                        }

                        final String dataTableFinal = dataTable;

                        JSONArray ruleParamiters = jsonRuleRequest.getJSONArray("Parameters");

                        Button btnRule = new Button(this);
                        if(isMonitoredUser){
                            btnRule.setText(Integer.toString(createdBy) + ":\tRule Request on " + dataTable + " data." );
                        }else {
                            btnRule.setText(Integer.toString(actionOn) + ":\tRule Request on " + dataTable + " data.");
                        }
                        // set the on click event
                        btnRule.setOnClickListener(e -> {
                            Intent ruleRequestView = new Intent(this, RuleRequestView.class);
                            // pass the data to the UI
                            ruleRequestView.putExtra("IsMonitoredUser", isMonitoredUser);
                            ruleRequestView.putExtra("RuleID", ruleID);
                            ruleRequestView.putExtra("RuleName",ruleName);
                            ruleRequestView.putExtra("ActionName", actionName);
                            ruleRequestView.putExtra("ActionOn", actionOn);
                            ruleRequestView.putExtra("CreatedBy", createdBy);
                            ruleRequestView.putExtra("Authorised", authorised);
                            ruleRequestView.putExtra("DataTable", dataTableFinal);
                            ruleRequestView.putExtra("RuleParamiters", ruleParamiters.toString());

                            startActivity(ruleRequestView);
                        });

                        vllRequestsArea.addView(btnRule);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    return;
                }
                break;

            default:
                Utility.displayServerError(pendingAprovalThread,this);
                break;
        }


    }
}
