package com.example.luke.watchfuleye.UI;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.luke.watchfuleye.R;
import com.example.luke.watchfuleye.utility.ServerConnectionThread;
import com.example.luke.watchfuleye.utility.Utility;
import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

public class GraphActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph_view);

        Intent intent = getIntent();

        int actionOn = intent.getIntExtra("UserID", -1);
        char mode = intent.getCharExtra("Mode", '-');

        String method = null;
        String activityTitle = getString(R.string.app_name) + " - ";
        switch (mode){
            case 'H':
                method = "Heart-Rate-Information/";
                activityTitle += "Heart Rate" ;
                break;
            case 'A':
                method = "Accelerometer-Information/";
                activityTitle += "Activity";
                break;
            default:
                method = "";
                break;
        }

        Date now = new Date();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(now);
        calendar.add(Calendar.MONTH, -1);
        DateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
        TimeZone timeZone = calendar.getTimeZone();
        isoFormat.setTimeZone(timeZone);

        Date oneMonthEarlier = calendar.getTime();

        HashMap<String,String> parameters = ServerConnectionThread.getBasicQueryParameters();
        parameters.put("Start_Time", isoFormat.format(oneMonthEarlier));
        parameters.put("End_Time", isoFormat.format(now));
        try {
            ServerConnectionThread serverConnectionThread = new ServerConnectionThread(method + actionOn ,parameters,null, ServerConnectionThread.HttpMethodType.GET, true);

            serverConnectionThread.start();
            // update the title of the activist
            setTitle(activityTitle);

            serverConnectionThread.join();
            int httpStatusCode = serverConnectionThread.getResponseCode();

            switch (httpStatusCode){
                case 200:
                    /*
                     * http://www.android-graphview.org/simple-graph/
                     * 10/02/2018
                     * Used for setting up the graph
                     * */
                    JSONArray serverResponseData = new JSONArray(serverConnectionThread.getResponse());
                    DataPoint[] dataPoints = new DataPoint[serverResponseData.length()];

                    DateFormat fromISO = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

                    for(int counter = 0; counter < serverResponseData.length(); counter++){
                        JSONObject dataPointJson = serverResponseData.getJSONObject( counter );
                        double reading = dataPointJson.getDouble("Reading");
                        String dateAsString = dataPointJson.getString("Stamp");
                        /*
                        * https://stackoverflow.com/questions/43923364/date-parsing-yyyy-mm-ddthhmmss-in-android
                        * 11/02/2018
                        * Used to understand why the date was not being parsed and identify a solution
                        * */
                        Date timestamp = fromISO.parse( dateAsString.replaceAll("Z$", "+0000"));
                        dataPoints[counter] = new DataPoint(timestamp.getTime(), reading);
                    }
                    GraphView graphView = (GraphView) findViewById(R.id.graph);
                    LineGraphSeries<DataPoint> lineGraphSeries = new LineGraphSeries<>(dataPoints);
                    lineGraphSeries.setDrawDataPoints(true);

                    graphView.getGridLabelRenderer().setHumanRounding(true);
                    graphView.addSeries(lineGraphSeries);

                    graphView.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter(){
                        DateFormat graphDateFormat = new SimpleDateFormat("dd/MM/YY\nHH:mm:ss");
                        @Override
                        public String formatLabel(double value, boolean isValueX) {
                            if(isValueX){
                                return graphDateFormat.format(new Date((long)value));
                            }else{
                                return super.formatLabel(value, isValueX);
                            }

                        }
                    });
                    //graphView.getViewport().setScrollable(true);

                    //graphView.getGridLabelRenderer().setHumanRounding(false);

                    break;
                default:
                    Utility.displayServerError(serverConnectionThread,this);
                    break;
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
