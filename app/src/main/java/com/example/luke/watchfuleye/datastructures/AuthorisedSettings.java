package com.example.luke.watchfuleye.datastructures;

/**
 * Created by Luke on 18/01/2018.
 */
// singleton class
public class AuthorisedSettings extends UserSettings{
    private static AuthorisedSettings singletonObject = null;

    private String accessToken;


    private AuthorisedSettings(){
        super();
        this.accessToken ="";
    }
    private AuthorisedSettings(int userID, String accessToken, GpsLocation gpsLocation, boolean annonymiseGpsData, Perspective perspective){
        this.accessToken = accessToken;

    }
    private AuthorisedSettings(int userID, String accessToken){
        super(userID);
        this.accessToken = accessToken;
    }

    public static AuthorisedSettings getInstance(){
        if(AuthorisedSettings.singletonObject == null){
            AuthorisedSettings.singletonObject = new AuthorisedSettings();
        }
        return AuthorisedSettings.singletonObject;
    }

    public void setAccessToken(String accessToken){
        this.accessToken = accessToken;
    }


    public String getAccessToken() {
        return accessToken;
    }

    public UserSettings getUserSettings(){
        return new UserSettings(this.getUserID(),this.getHomeLocation(),this.isAnnonymiseGpsData(),this.getPerspective(),this.isAllowMovementAndHeartRateRecordings(),this.isAllowGpsRecordings());
    }

    public void reset(){
        singletonObject = new AuthorisedSettings();
    }
}
