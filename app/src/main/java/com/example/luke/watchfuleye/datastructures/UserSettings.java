package com.example.luke.watchfuleye.datastructures;

/**
 * Created by Luke on 11/02/2018.
 */

public class UserSettings {
    private int userID;
    private GpsLocation homeLocation;
    private boolean annonymiseGpsData;
    private Perspective perspective;

    private boolean allowMovementAndHeartRateRecordings;
    private boolean allowGpsRecordings;

    protected UserSettings(){
        this.userID = -1;
        this.homeLocation = null;
        this.annonymiseGpsData = true;
        this.perspective = null;
        this.allowGpsRecordings = false;
        this.allowMovementAndHeartRateRecordings = false;
    }



    protected UserSettings(int userID) {
        this.userID = userID;
        this.homeLocation = null;
        this.annonymiseGpsData = false;
        this.perspective = null;
        this.allowMovementAndHeartRateRecordings = false;
        this.allowGpsRecordings = false;
    }

    public UserSettings(int userID, GpsLocation homeLocation, boolean annonymiseGpsData, Perspective perspective, boolean allowMovementAndHeartRateRecordings, boolean allowGpsRecordings) {
        this.userID = userID;
        this.homeLocation = homeLocation;
        this.annonymiseGpsData = annonymiseGpsData;
        this.perspective = perspective;
        this.allowMovementAndHeartRateRecordings = allowMovementAndHeartRateRecordings;
        this.allowGpsRecordings = allowGpsRecordings;
    }


    public void setUserID(int userID){
        this.userID = userID;
    }
    public void setHomeLocation(GpsLocation location){
        this.homeLocation = location;
    }
    public void setAnnonymiseGpsData(boolean annonymiseGpsData){
        this.annonymiseGpsData = annonymiseGpsData;
    }
    public void setPerspective(Perspective perspective){
        this.perspective = perspective;
    }

    public boolean isAllowMovementAndHeartRateRecordings() {
        return allowMovementAndHeartRateRecordings;
    }

    public void setAllowMovementAndHeartRateRecordings(boolean allowMovementAndHeartRateRecordings) {
        this.allowMovementAndHeartRateRecordings = allowMovementAndHeartRateRecordings;
    }

    public boolean isAllowGpsRecordings() {
        return allowGpsRecordings;
    }

    public void setAllowGpsRecordings(boolean allowGpsRecordings) {
        this.allowGpsRecordings = allowGpsRecordings;
    }

    public int getUserID() {
        return userID;
    }
    public boolean isAnnonymiseGpsData(){
        return this.annonymiseGpsData;
    }
    public GpsLocation getHomeLocation(){
        return this.homeLocation;
    }

    public Perspective getPerspective(){
        return  this.perspective;
    }
}
