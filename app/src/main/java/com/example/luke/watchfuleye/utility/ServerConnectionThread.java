package com.example.luke.watchfuleye.utility;

import android.content.res.Resources;

import com.example.luke.watchfuleye.R;
import com.example.luke.watchfuleye.datastructures.AuthorisedSettings;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

/**
 * Created by Luke on 21/12/2017.
 */

public class ServerConnectionThread extends Thread {
    protected URL url;
    protected HttpMethodType method;
    protected String response;
    protected ArrayList<String> errors;
    protected JSONObject body;
    protected int httpStatusCode;
    protected boolean requiresAuthentication;

    protected static SSLContext sslContext;
    public enum HttpMethodType{
        GET,
        POST,
        DELETE
    }
    public ServerConnectionThread(String method, Map<String,String> queryParameters, JSONObject body, HttpMethodType methodType, boolean requiresAuthentication) throws MalformedURLException {
        String url = "https://watchful-eye.l-chapman.co.uk/" + method;///*"http://192.168.1.7:3000/" + method; */ "https://192.168.0.16:3000/" + method; //"http://10.30.59.23:3000/" + method;
        this.method = methodType;
        int counter = 0;
        for ( Map.Entry<String,String> entry: queryParameters.entrySet()){
            if(counter == 0){
                url += '?';
            }else{
                url += '&';
            }
            url += entry.getKey() + '=' + entry.getValue();
            counter++;
        }
        this.url = new URL(url);

        errors = new ArrayList<>();
        this.body = body;
        this.response = "";
        this.requiresAuthentication = requiresAuthentication;
    }

    public static void setupTrustCertificate(Resources resources) throws CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        /*
         *https://developer.android.com/training/articles/security-ssl.html
          *
           * 27/02/2018
           *
           * Used in order to allow self signed certificate to be trusted
         */


        // Load CAs from an InputStream
// (could be from a resource or ByteArrayInputStream or ...)
        CertificateFactory cf = CertificateFactory.getInstance(("X.509"));
// From https://www.washington.edu/itconnect/security/ca/load-der.crt
        InputStream caInput = resources.openRawResource(R.raw.cert);
        Certificate ca;
        try {
            ca = cf.generateCertificate(caInput);
            System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
        } finally {
            caInput.close();
        }

// Create a KeyStore containing our trusted CAs
        String keyStoreType = KeyStore.getDefaultType();
        KeyStore keyStore = KeyStore.getInstance(keyStoreType);
        keyStore.load(null, null);
        keyStore.setCertificateEntry("ca", ca);

// Create a TrustManager that trusts the CAs in our KeyStore
        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        tmf.init(keyStore);

// Create an SSLContext that uses our TrustManager
        SSLContext context = SSLContext.getInstance("TLS");
        context.init(null, tmf.getTrustManagers(), null);
        sslContext = context;
    }

    public String getResponse(){
        return this.response;
    }

    public ArrayList<String> getErrors(){
        return this.errors;
    }

    public int getResponseCode(){
        return this.httpStatusCode;
    }

    @Override
    public void run() {

        HttpsURLConnection urlConnection = null;

        try {
            urlConnection = (HttpsURLConnection) this.url.openConnection();
            urlConnection.setSSLSocketFactory(sslContext.getSocketFactory());
            urlConnection.setConnectTimeout(1000);
            switch (this.method){
                case GET:
                    urlConnection.setRequestMethod("GET");
                    break;
                case POST:
                    urlConnection.setRequestMethod("POST");
                    break;
                case DELETE:
                    urlConnection.setRequestMethod("DELETE");
                    break;
            }
            urlConnection.setDoInput(true);

            if(requiresAuthentication){
                urlConnection.setRequestProperty("AccessToken",AuthorisedSettings.getInstance().getAccessToken());
            }

            if(body != null){ // if there is a body then post stuff
                urlConnection.setRequestProperty("Content-Type", "application/json");
                String bdy = this.body.toString();
                urlConnection.setDoOutput(true);
                urlConnection.connect();
                OutputStream outputStream = urlConnection.getOutputStream();
                PrintWriter outputWriter = new PrintWriter(outputStream);
                outputWriter.print(this.body.toString());
                outputWriter.close();
                outputStream.close();
            }else {
                urlConnection.setDoOutput(false);
                urlConnection.connect();
            }

            this.httpStatusCode = urlConnection.getResponseCode();

            InputStream inputStream = null;
            if(this.httpStatusCode >= 200 && this.httpStatusCode < 400){
                inputStream = urlConnection.getInputStream();
            }else{
                inputStream = urlConnection.getErrorStream();
            }


            BufferedReader inputReader = new BufferedReader(new InputStreamReader(inputStream));

            String input = null;

            while ((input = inputReader.readLine()) != null){
                this.response += input;
            }


        } catch (IOException e) {
            e.printStackTrace();
            this.errors.add(e.toString());
        }

    }

    public static HashMap<String,String> getBasicQueryParameters(){
        return new HashMap<>();
    }
    public static HashMap<String,String> getBasicQueryParametersWithAuthentication(){
        HashMap<String,String> queryParams = getBasicQueryParameters();
        //queryParams.put("AccessToken", AuthorisedSettings.getInstance().getAccessToken());
        return queryParams;
    }
}
