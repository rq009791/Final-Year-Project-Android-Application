package com.example.luke.watchfuleye.datastructures;

/**
 * Created by Luke on 18/01/2018.
 */

public class GpsLocation {
    private double longitude;
    private double latitude;

    public GpsLocation(double longitude, double latitude){
        setLatitude(latitude);
        setLongitude(longitude);
    }

    //https://stackoverflow.com/questions/15965166/what-is-the-maximum-length-of-latitude-and-longitude -> 18/01/2018 -> JasonM1 , Ben Davison
    private void setLongitude(double longitude){ // -180 to 180
      if(longitude < - 180 || longitude > 180){ // if invalid then raise an error
        throw new IllegalArgumentException("Longitude must be between -180 and 180.");
      }
      this.longitude = longitude;
    }
    private void setLatitude(double latitude){ // -90 to  90
        if(latitude < - 90 || latitude > 90){ // if invalid then raise an error
            throw new IllegalArgumentException("Longitude must be between -90 and 90.");
        }
        this.latitude = latitude;
    }
    public double getLongitude(){
        return longitude;
    }
    public double getLatitude(){
        return latitude;
    }
}
