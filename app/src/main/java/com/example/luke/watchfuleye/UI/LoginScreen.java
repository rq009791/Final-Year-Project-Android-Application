package com.example.luke.watchfuleye.UI;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.luke.watchfuleye.R;
import com.example.luke.watchfuleye.datastructures.AuthorisedSettings;
import com.example.luke.watchfuleye.datastructures.GpsLocation;
import com.example.luke.watchfuleye.datastructures.Perspective;
import com.example.luke.watchfuleye.datastructures.UserSettings;
import com.example.luke.watchfuleye.utility.ServerConnectionThread;
import com.example.luke.watchfuleye.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/*18/01/2018
https://stackoverflow.com/questions/31564907/how-to-manage-oauth-access-token-in-android-and-its-flow-between-activities -> fname = Gianluca
* https://stackoverflow.com/questions/1944369/android-static-object-lifecycle -> fname = Samuh
* */
public class LoginScreen extends AppCompatActivity {
    private static File usersFile;
    private static File connectionsFile;
    private  EditText username;
    private TextView usernameLabel;
    private EditText password;

    private Button btnlogin;
    private Button btnRegister;

    private Spinner spnUserType;

    private LinearLayout hllUserID;
    private TextView lblUserType;

    // user settings methods

    private static HashMap<Integer,UserSettings> readUsers(){
        HashMap<Integer,UserSettings> userSettingsArrayList = new HashMap<>();
        // read in the current users file which includes their current settings
        if(usersFile.exists() && usersFile.isFile()){
            String fileData = "";

            try {
                Scanner input = new Scanner(usersFile);
                while (input.hasNextLine()){
                    fileData += input.nextLine();
                }
                input.close();
                JSONArray usersFileArray = new JSONArray(fileData);

                for(int counter = 0; counter < usersFileArray.length(); counter++){
                    JSONObject userSettingsJson = usersFileArray.getJSONObject(counter);

                    int userID = userSettingsJson.getInt("UserID");
                    boolean anonymiseGpsData = userSettingsJson.getBoolean("AnonymiseGpsData");
                    boolean allowGpsRecordings = userSettingsJson.getBoolean("AllowGpsRecordings");
                    boolean allowMovementAndHeartRateRecordings = userSettingsJson.getBoolean("AllowMovementAndHeartRateRecordings");
                    Object homeLocationJson = userSettingsJson.get("HomeLocation");
                    GpsLocation homeLocation = null;
                    // if one is not a home loaction the leave it null
                    if(!homeLocationJson.equals(JSONObject.NULL)){
                        JSONObject homeLocationJsonObject = (JSONObject)homeLocationJson;
                        double latitude = homeLocationJsonObject.getDouble("Latitude");
                        double longitude = homeLocationJsonObject.getDouble("Longitude");
                        homeLocation = new GpsLocation(longitude,latitude);
                    }


                    Perspective perspective = null;
                    Object perspectiveObj = userSettingsJson.get("Perspective");
                    if(!perspectiveObj.equals(JSONObject.NULL)){
                        String perspectiveString = (String)perspectiveObj;

                        switch (perspectiveString){
                            case "DEFAULT":
                                perspective = Perspective.DEFAULT;
                                break;
                            case  "FAMILY" :
                                perspective = Perspective.FAMILY;
                                break;
                            case  "MEDICAL_PROFESSIONAL" :
                                perspective = Perspective.MEDICAL_PROFESSIONAL;
                                break;
                            default:
                                // non needed default
                                break;
                        }
                    }

                    UserSettings userSettings = new UserSettings(userID,homeLocation,anonymiseGpsData,perspective,allowMovementAndHeartRateRecordings,allowGpsRecordings);
                    userSettingsArrayList.put(userID,userSettings);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        return userSettingsArrayList;
    }

    public static void updateUserSettings(UserSettings userSettings){
        HashMap<Integer,UserSettings> userSettingsMap  = readUsers();
        // add or update the record
        userSettingsMap.put(userSettings.getUserID(), userSettings);

        JSONArray jsonArray = new JSONArray();

        for (UserSettings userSettingsMapItem : userSettingsMap.values()) {



            try {
                JSONObject userSettingsJson = new JSONObject();
                userSettingsJson.put("UserID", userSettingsMapItem.getUserID());
                userSettingsJson.put("AnonymiseGpsData", userSettingsMapItem.isAnnonymiseGpsData());
                userSettingsJson.put("AllowGpsRecordings", userSettingsMapItem.isAllowGpsRecordings());
                userSettingsJson.put("AllowMovementAndHeartRateRecordings", userSettingsMapItem.isAllowMovementAndHeartRateRecordings());
                GpsLocation homeLocation = userSettings.getHomeLocation();
                JSONObject homeLocationJson = null;
                if(homeLocation != null){
                    homeLocationJson = new JSONObject();
                    homeLocationJson.put("Latitude", homeLocation.getLatitude());
                    homeLocationJson.put("Longitude", homeLocation.getLongitude());
                    userSettingsJson.put("HomeLocation", homeLocationJson);
                }else{
                    userSettingsJson.put("HomeLocation", JSONObject.NULL);
                }

                String perspectiveString = null;
                Perspective perspective = userSettingsMapItem.getPerspective();
                if(perspective != null){
                    switch (perspective){
                        case DEFAULT:
                            perspectiveString = "DEFAULT";
                            break;
                        case  FAMILY:
                            perspectiveString = "FAMILY";
                            break;
                        case  MEDICAL_PROFESSIONAL:
                            perspectiveString = "MEDICAL_PROFESSIONAL";
                            break;
                    }
                }
                if(perspectiveString == null){
                    userSettingsJson.put("Perspective", JSONObject.NULL);
                }else{
                    userSettingsJson.put("Perspective", perspectiveString);
                }


                // now add the overall json to the array outside the loop
                jsonArray.put(userSettingsJson);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        try {
            PrintWriter output = new PrintWriter(new FileWriter(usersFile,false));
            output.println(jsonArray.toString());
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    // connections methods

    public static HashMap<Integer,String> readConnections(){
        HashMap<Integer,String> connectionsMap = new HashMap<>();
        try {
            Scanner input = new Scanner(connectionsFile);
            String fileString = "";
            while (input.hasNextLine()){
                fileString += input.nextLine();
            }
            input.close();

            JSONArray fileJsonArray = new JSONArray(fileString);
            for(int counter = 0; counter < fileJsonArray.length(); counter++){
                JSONObject element = fileJsonArray.getJSONObject(counter);
                int userID = element.getInt("UserID");
                String commonName = element.getString("CommonName");
                connectionsMap.put(userID, commonName);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return connectionsMap;
    }

    public static void updateConnections(int userID, String commonName){

        HashMap<Integer,String> connections = readConnections();

        // replace the userID or add it if it does not exist
        connections.put(userID,commonName);

        JSONArray jsonArray = new JSONArray();

        for(Map.Entry<Integer,String> entry : connections.entrySet()){
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("UserID", entry.getKey());
                jsonObject.put("CommonName", entry.getValue());
                jsonArray.put(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        try {

            // construct an output object
            PrintWriter output = new PrintWriter(connectionsFile);
            output.write(jsonArray.toString());
            output.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.username = findViewById(R.id.usernameField);
        this.password = findViewById(R.id.passwordField);


        this.btnlogin = findViewById(R.id.btnLogin);
        this.btnRegister = findViewById(R.id.btnRegister);
        this.usernameLabel = findViewById(R.id.lblUsername);

        this.hllUserID =  (LinearLayout)findViewById(R.id.hllUserID);
        this.lblUserType = (TextView)findViewById(R.id.lblUserType);


        this.spnUserType = (Spinner)findViewById(R.id.spnUserType);
        ArrayAdapter userTypeAdapter = ArrayAdapter.createFromResource(this, R.array.Perspective, android.R.layout.simple_spinner_item);
        userTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnUserType.setAdapter(userTypeAdapter);

        try {
            ServerConnectionThread.setupTrustCertificate(this.getResources());
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

        this.btnlogin.setOnClickListener(e ->{
            this.btnRegister.setVisibility(View.VISIBLE);
            this.btnlogin.setVisibility(View.GONE);
            this.usernameLabel.setVisibility(View.VISIBLE);
            this.username.setVisibility(View.VISIBLE);
            this.hllUserID.setVisibility(View.VISIBLE);
            this.lblUserType.setVisibility(View.GONE);
            this.spnUserType.setVisibility(View.GONE);
        });

        this.btnRegister.setOnClickListener(e ->{
            this.btnlogin.setVisibility(View.VISIBLE);
            this.btnRegister.setVisibility(View.GONE);
            this.usernameLabel.setVisibility(View.GONE);
            this.username.setVisibility(View.GONE);
            this.hllUserID.setVisibility(View.GONE);
            this.lblUserType.setVisibility(View.VISIBLE);
            this.spnUserType.setVisibility(View.VISIBLE);
        });

        this.btnlogin.setVisibility(View.GONE);

        // setting up files
        if(usersFile == null){
            usersFile = new File(this.getFilesDir(), "usersSettings.uset");
        }
        if(connectionsFile == null){
            connectionsFile = new File(this.getFilesDir(), "deviceConnections.uset");
        }

        HashMap<Integer, UserSettings> usersMap = readUsers();
        ArrayList<String> usersList = new ArrayList<>();

        for( UserSettings userSettings : usersMap.values()){
            usersList.add(Integer.toString(userSettings.getUserID()));
        }

        Spinner spinner = (Spinner)findViewById(R.id.spnUsers);
        ArrayAdapter<String> usersSpinnerAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, usersList);

        usersSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(usersSpinnerAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                username.setText((String)parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // do nothing
            }
        });
    }

    public void login(View view) {
        ArrayList<String> errors = new ArrayList<String>();
        if (username.getText().toString().equals("") && this.btnRegister.getVisibility() == View.VISIBLE) {
            errors.add("You must enter a valid user name. This should be a whole number such as 1,2,3,4...");
        }
        if (password.getText().toString().equals("")) {
            errors.add("You must enter a valid password. This must be at least a character long.");
        }





        // prepare a dialog box for later
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        if(errors.size() > 0){
            alertDialog.setTitle("Error!");
            alertDialog.setMessage(errors.toString());
            alertDialog.show();
        }else{

            Perspective perspective  = null;
            String stringPerspective = null;

            switch (this.spnUserType.getSelectedItemPosition()){
                case 0:
                    perspective = Perspective.DEFAULT;
                    stringPerspective = "D";
                    break;
                case 1:
                    perspective = Perspective.FAMILY;
                    stringPerspective = "F";
                    break;
                case 2:
                    perspective = Perspective.MEDICAL_PROFESSIONAL;
                    stringPerspective = "M";
                    break;
            }

            ServerConnectionThread serverConnection = null;
            JSONObject credentials = new JSONObject();
            try {
                if(this.btnRegister.getVisibility() == View.GONE){

                    credentials.put("UserType", stringPerspective);
                }else{
                    credentials.put("Username", Integer.valueOf(username.getText().toString()));
                }
                credentials.put("Password", password.getText().toString());


                switch (this.btnRegister.getVisibility()){
                    case View.VISIBLE:
                        serverConnection = new ServerConnectionThread("Login", new HashMap<String, String>(), credentials, ServerConnectionThread.HttpMethodType.POST, false);
                        break;
                    case View.GONE:
                        serverConnection = new ServerConnectionThread("Register", new HashMap<String, String>(), credentials, ServerConnectionThread.HttpMethodType.POST,false);
                        break;
                }

                serverConnection.start();

                // reset the privileges
                AuthorisedSettings.getInstance().reset();

                // wait until thread finishes
                serverConnection.join();
                errors.addAll(serverConnection.getErrors());

            } catch (JSONException e) {
                e.printStackTrace();
                errors.add(e.toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
                errors.add(e.toString());
            } catch (InterruptedException e) {
                e.printStackTrace();
                errors.add(e.toString());
            }
            if (errors.size() > 0) {

                alertDialog.setTitle("Error!");
                alertDialog.setMessage(errors.toString());
                alertDialog.show();
            } else {
                // check the response from the server
                if(serverConnection.getResponseCode() == 200){ // if successful http request then load the next page
                    try {
                        JSONObject apiResponse = new JSONObject(serverConnection.getResponse());
                        AuthorisedSettings authorisedSettings = AuthorisedSettings.getInstance();
                        authorisedSettings.setUserID(apiResponse.getInt("UserID"));
                        authorisedSettings.setAccessToken(apiResponse.getString("Token"));

                        // check to see if the user settings already exists
                        HashMap<Integer,UserSettings> userSettingsHashMap = readUsers();

                        if(userSettingsHashMap.containsKey(authorisedSettings.getUserID())) { // if authorisedSettings already exist for this user
                            UserSettings userSettings = userSettingsHashMap.get(authorisedSettings.getUserID());
                            authorisedSettings.setAllowGpsRecordings(userSettings.isAllowGpsRecordings());
                            authorisedSettings.setPerspective(userSettings.getPerspective());
                            authorisedSettings.setAllowMovementAndHeartRateRecordings(userSettings.isAllowMovementAndHeartRateRecordings());
                            authorisedSettings.setHomeLocation(userSettings.getHomeLocation());
                            authorisedSettings.setAnnonymiseGpsData(userSettings.isAnnonymiseGpsData());
                            Intent mainMenu = new Intent(this,MainMenu.class);
                            startActivity(mainMenu);
                        }else{
                            if(this.btnRegister.getVisibility() == View.GONE){
                                authorisedSettings.setPerspective(perspective);
                            }
                            Intent mainMenu = new Intent(this,SettingsActivity.class);
                            startActivity(mainMenu);
                        }
                    } catch (JSONException e) {
                        alertDialog.setTitle("Error!");
                        alertDialog.setMessage("Application Error: Unexpected response received from server");
                        alertDialog.show();
                    }

                }else {
                    Utility.displayServerError(serverConnection,this);
                }
            }
        }

    }




}
