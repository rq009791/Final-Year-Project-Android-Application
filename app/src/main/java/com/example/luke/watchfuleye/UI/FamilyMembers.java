package com.example.luke.watchfuleye.UI;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.luke.watchfuleye.R;
import com.example.luke.watchfuleye.utility.ServerConnectionThread;
import com.example.luke.watchfuleye.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.HashMap;

public class FamilyMembers extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family_members);


        LinearLayout vllFamilyMembers = (LinearLayout) findViewById(R.id.vllFamilyMembersList);

        HashMap<String,String> queryParameters = ServerConnectionThread.getBasicQueryParameters();
        queryParameters.put("Relation","F");

        try {
            ServerConnectionThread serverConnectionThread = new ServerConnectionThread("Monitored-Users", queryParameters, null, ServerConnectionThread.HttpMethodType.GET, true  );
            serverConnectionThread.start();

            serverConnectionThread.join();

            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            switch (serverConnectionThread.getResponseCode()){
                case 200:
                    JSONArray jsonArray = new JSONArray(serverConnectionThread.getResponse());
                    /*alertDialog.setTitle("Response");
                    alertDialog.setMessage(jsonArray.toString());
                    alertDialog.show();*/

                    for(int counter = 0; counter < jsonArray.length(); counter++){
                        final JSONObject object = jsonArray.getJSONObject(counter);

                        Button btnUser = new Button(this);
                        final int userID = object.getInt("Monitored_UserID");
                        btnUser.setText("" + userID);
                        btnUser.setOnClickListener(e ->{
                            /*alertDialog.setTitle("Button Clicked" );
                            alertDialog.setMessage("" + userID);
                            alertDialog.show();*/
                            Intent warningsView = new Intent(this,Warnings.class);
                            warningsView.putExtra("UserID", userID);
                            startActivity(warningsView);
                        });
                        vllFamilyMembers.addView(btnUser);
                    }

                    break;
                default:
                    Utility.displayServerError(serverConnectionThread,this);
                    break;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}
